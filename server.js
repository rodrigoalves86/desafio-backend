'use strict';
const express = require('express');
const app = express();
const { Routes } = require('./app/routes');
const bodyParser = require('body-parser');
const token = require('./app/auth/');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

app.use(token.auth);

Routes(app);

app.get('/status', (req, res) => {
  res.send(`API está ok e funcionando na porta ${PORT}`)
})

const PORT = process.env.PORT || 3000;
const server = app.listen(PORT, () => console.log(`Servidor iniciado na porta ${PORT}`));