'use strict';

const cache = require('memory-cache');
exports.cacheAPI = (duration_seconds) => {
  return (req, res, next) => {
    const key = '__express__' + (req.originalUrl || req.url) + JSON.stringify(req.body);
    const cachedBody = cache.get(key);
    // Check if body is cached
    if(cachedBody){
      // Send response using cached body
      res.send(JSON.parse(cachedBody));
    }
    else {
      // Continue the pipeline to fulfill the request, saving the body before finishing response
      res.sendResponse = res.send;
      res.send = (body) => {
        cache.put(key, body, duration_seconds*1000);
        res.sendResponse(body);
      }
      next();
    }
  }
};