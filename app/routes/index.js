const { getLicenses, submitLicense } = require('../controllers/medical_license')
const { getUsers, saveUser } = require('../controllers/users')
const { getEmployer, getMemberByPinCode } = require('../controllers/employer')
const { cacheAPI } = require('../cache/memory_cache');

exports.Routes = (app) => {
  app.route('/users/:id?', cacheAPI(30))
     .get((req, res) => {
       res.send(getUsers())
     })
     .post((req, res) => {
        const users = getUsers()

        users.push(req.body)
        saveUser(users)

        res.status(201).send(req.body)
     })
  
  app.route('/medical-licenses/', cacheAPI(30))
     .get((req, res) => {
        const data = getLicenses()
        // console.log('data: ', data);
  
        // const newData = data.map((row) => {
        //   delete row['member_code'];
        // });
        // console.log('data: ', newData);

        res.send(data)
     })

  app.route('/submit-medical-license', cacheAPI(30))
     .post((req, res) => {
        const ml = getLicenses()

        ml.push(req.body)
        submitLicense(ml)

        res.status(201).send(`Sucess: ${JSON.stringify(req.body)}`);
    })
  
  app.route('/employer/:employerCode', cacheAPI(30))
     .get(getEmployer)

  app.route('/check-pin/:employerCode', cacheAPI(30))
     .post(getMemberByPinCode)
}
