'use strict';
const fs = require('fs');

exports.getData = (databaseName) => {
  try {
    let rawData = fs.readFileSync('database/' + databaseName + '.json');
    let db = JSON.parse(rawData);
    return db;
  } catch (err){
    console.log(err);
  }
}

exports.saveData = (db, databaseName) => {
  try {
    let stringData = JSON.stringify(db, null, 2);
    fs.writeFileSync('database/' + databaseName + '.json', stringData);
  } catch (err){
    console.log(err);
  }
}