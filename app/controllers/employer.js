'use strict';
const fs = require('fs');
const { join } = require('path')

const database = join(__dirname, '../data/employer.json')

exports.getEmployer = async (req, res) => {
  try {
    let data = fs.existsSync(database)
    ? JSON.parse(fs.readFileSync(database))
    : []
    
    let employer = data.filter((row) => {
      return row.employer_code === req.params.employerCode;
    });

    if(employer.length > 0){
      employer = employer[0];
    }

    let code = employer.members[0]

    const { pin_code } = code
    
    res.status(200).send({pin_code});
  }
  catch (error){
    res.status(400).send(error);
  }
}

exports.getMemberByPinCode = async (req, res) => {
  let { pin_code } = req.body
  
  try {
    let data = fs.existsSync(database)
    ? JSON.parse(fs.readFileSync(database))
    : []

    let employer = data.filter((row) => {
      return row.employer_code === req.params.employerCode;
    });

    if(employer.length > 0){
      employer = employer[0];
      console.log('employer: ', employer);
    }

    let member = employer['members'].filter((row) => {
      return row.pin_code == pin_code;
    });

    if(member.length > 0){
      member = member[0];
    }

    delete member['pin_code'];

    res.status(200).send(member);
  }
  catch (error){
    res.status(400).send(error);
  }
}