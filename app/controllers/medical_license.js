'use strict';
const fs = require('fs');
const { join } = require('path')

const database = join(__dirname, '../data/medical_license.json')

exports.getLicenses = () => {
  const data = fs.existsSync(database)
  ? JSON.parse(fs.readFileSync(database))
  : []

  data.map((row) => {
    delete row['member_code'];
  });

  try {
    return data
  }
  catch (error){
    console.error(error);
  }
}

exports.submitLicense = (licenses) => fs.writeFileSync(database, JSON.stringify(licenses, null, '\t'))

