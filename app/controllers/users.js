'use strict';
const fs = require('fs')
const { join } = require('path')

const database = join(__dirname, '../data/users.json')

exports.getUsers = () => {
  const data = fs.existsSync(database)
  ? fs.readFileSync(database)
  : []

  try {
    return JSON.parse(data)
  } catch (error) {
    return []
    console.error(error)
  }
}

exports.saveUser = (users) => fs.writeFileSync(database, JSON.stringify(users, null, '\t'))
