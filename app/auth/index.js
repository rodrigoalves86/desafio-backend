'use strict';

exports.auth = async (req, res, next) => {

  const credentials = {
    'login': 'qrpoint',
    'pass': 'qrpoint'
  };

  if (req.headers.authorization === undefined) {
    res.status(401).send({
      'message': 'Authorization required'
    });
    return;
  }

  const base64 = req.headers.authorization.split(' ')[1];

  const [login, pass] = Buffer.from(base64, 'base64').toString('ascii').split(':');

  if (login === credentials['login'] && pass == credentials['pass']) {
    next();
  } else {
    res.status(403).send({
      'message': 'user does not exist or it is not allowed to access this resource'
    });
  }
}